#pragma once
#include "Scene.h"
class ConcertListScene : public Scene {
private:
	enum STATE { NONE, INTRO, LIST, SUB_STROY, POINT_REWARD };
public:
	ConcertListScene();
	~ConcertListScene();


	bool CheckFirst() override;
	bool CheckScene() override;
	bool ReadData() override;
	void ActionDecision() override;


private:
	static std::string GetNumber(int x, int y, int width, int height);

	void AddConcertTodo();
	void AddPointRewardTodo();
private:
	STATE m_state = NONE;
	bool isQuit = false;
	bool isWait = false;
};

